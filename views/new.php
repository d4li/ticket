<form method="POST">
  <div class="panel panel-default">
    <button type="submit" name="save" class="save btn btn-success pull-right disabled">Salvar</button>
    <a href="<?php echo BASEURL; ?>?page=all" class="btn btn-md btn-danger pull-right">Cancelar</a>
    <div class="panel-heading">
      <h3 class="panel-title">Novo Ticket</h3>
    </div>
    <div class="panel-body">
      <div class="form-group">
        <label for="firstname">Nome </label><span class="required hide text-danger pull-right">* Campo obrigatório</span>
        <input type="text" class="form-control" id="firstname" name="firstname" placeholder="Nome">
      </div>
      <div class="form-group">
        <label for="lastname">Sobrenome</label><span class="required hide text-danger pull-right">* Campo obrigatório</span>
        <input type="text" class="form-control" id="lastname" name="lastname" placeholder="Sobrenome">
      </div>
      <div class="form-group">
        <label for="category">Categoria</label><span class="required hide text-danger pull-right">* Campo obrigatório</span>
        <select id="category" id="category" name="category" class="form-control">
          <option>BUG</option>
          <option>NEW</option>
          <option>UPDATE</option>
        </select>
      </div>
      <div class="form-group">
        <label for="priority">Prioridade</label><span class="required hide text-danger pull-right">* Campo obrigatório</span>
        <select id="priority" id="priority" name="priority" class="form-control">
          <option>BAIXA</option>
          <option>MEDIA</option>
          <option>ALTA</option>
        </select>
      </div>
      <div class="form-group">
        <label for="description">Descrição</label><span class="required hide text-danger pull-right">* Campo obrigatório</span>
        <textarea class="form-control" id="description" name="description" rows="8" cols="80"></textarea>
      </div>
      <button type="submit" name="save" class="save btn btn-success pull-right disabled">Salvar</button>
    </div>
  </div>
</form>
