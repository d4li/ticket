<?php include_once DBAPI; ?>
<?php
  session_start();
  // echo '<pre>';
  // var_dump($_REQUEST);
  // var_dump($_SESSION);
  // var_dump($_COOKIE);
  // echo '</pre>';
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Ticket</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="<?php echo ASSETS; ?>css/bootstrap.min.css">

    <!-- Optional theme -->
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"> -->

    <link rel="stylesheet" href="<?php echo BASEURL; ?>assets/css/main.css">
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.2/css/font-awesome.min.css"> -->
  </head>
  <body>
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a href="<?php echo BASEURL; ?>" class="navbar-brand">Tickets</a>
        </div>
      </div>
    </nav>

    <main class="container">
      <?php if(isset($_SESSION['getMessage']) && isset($_SESSION['typeMessage'])){ ?>
      <div class="alert alert-<?php echo $_SESSION['typeMessage']; ?> alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <?php echo $_SESSION['getMessage']; ?>.
      </div>
      <?php } ?>
