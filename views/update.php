<form method="POST">
  <input type="hidden" name="flag" value="<?php echo $tickets[0]['flag']; ?>" />
  <div class="panel panel-default">
    <button type="submit" name="save" class="save btn btn-success pull-right">Salvar</button>
    <a href="<?php echo BASEURL; ?>?page=all" class="btn btn-md btn-danger pull-right">Cancelar</a>
    <div class="panel-heading">
      <h3 class="panel-title">Editando Ticket #<?php echo $tickets[0]['id']; ?></h3>
    </div>
    <div class="panel-body">
      <div class="form-group">
        <label for="firstname">Nome</label>
        <input type="text" class="form-control" id="firstname" name="firstname" placeholder="Nome" value="<?php echo $tickets[0]['firstname']; ?>">
      </div>
      <div class="form-group">
        <label for="lastname">Sobrenome</label>
        <input type="text" class="form-control" id="lastname" name="lastname" placeholder="Sobrenome" value="<?php echo $tickets[0]['lastname']; ?>">
      </div>
      <div class="form-group">
        <label for="category">Categoria</label>
        <select id="category" name="category" class="form-control">
          <option<?php if($tickets[0]['category'] == 'BUG'): echo ' selected'; endif; ?>>BUG</option>
          <option<?php if($tickets[0]['category'] == 'NEW'): echo ' selected'; endif; ?>>NEW</option>
          <option<?php if($tickets[0]['category'] == 'UPDATE'): echo ' selected'; endif; ?>>UPDATE</option>
        </select>
      </div>
      <div class="form-group">
        <label for="priority">Categoria</label>
        <select id="priority" name="priority" class="form-control">
          <option<?php if($tickets[0]['priority'] == 'BAIXA'): echo ' selected'; endif; ?>>BAIXA</option>
          <option<?php if($tickets[0]['priority'] == 'MEDIA'): echo ' selected'; endif; ?>>MEDIA</option>
          <option<?php if($tickets[0]['priority'] == 'ALTA'): echo ' selected'; endif; ?>>ALTA</option>
        </select>
      </div>
      <div class="form-group">
        <label for="description">Descrição</label>
        <textarea class="form-control" name="description" rows="8" cols="80"><?php echo $tickets[0]['description']; ?></textarea>
      </div>
      <button type="submit" name="save" class="save btn btn-success pull-right">Salvar</button>
    </div>
  </div>
</form>
