<div class="panel panel-default">
  <a href="<?php echo BASEURL; ?>?page=new" class="btn btn-success pull-right">Novo Ticket</a>
  <div class="panel-heading">
    <h3 class="panel-title">Todos os Tickets</h3>
  </div>
  <div class="panel-body">
    <table class="table table-hover table-bordered table-striped table-responsive">
      <ul class="nav nav-tabs">
        <li role="presentation"<?php if(isset($_GET['flag']) && $_GET['flag'] == '0' || !isset($_GET['flag'])): echo ' class="active"'; endif; ?>><a href="?flag=0">Todos</a></li>
        <li role="presentation"<?php if(isset($_GET['flag']) && $_GET['flag'] == '1'): echo ' class="active"'; endif; ?>><a href="?flag=1">Arquivados</a></li>
      </ul>
      <thead>
        <th>#</th>
        <th>Nome Completo</th>
        <th>Descrição</th>
        <th>Categoria</th>
        <th>Prioridade</th>
        <th>Data/Hora</th>
      </thead>
      <tbody>
        <?php
          if($tickets){
            foreach($tickets as $item){
              if($item['flag'] !== '1') : $flag='1'; else: echo '<p'.$item['flag'].'</p>'; $flag='0'; endif;
              echo '<tr>';
                echo '<td>';
                echo '</td>';
                echo '<td>';
                  echo '<a href="'.BASEURL.'?page=update&id='.$item['id'].'" class="text-info" title="Editar Ticket">';
                  echo strtoupper($item['firstname']).' '.strtoupper($item['lastname']);
                  echo '</a>';
                echo '</td>';
                echo '<td>';
                  echo $item['description'];
                echo '</td>';
                echo '<td>';
                  echo $item['category'];
                echo '</td>';
                echo '<td>';
                  echo $item['priority'];
                echo '</td>';
                echo '<td>';
                  $date_create = new DateTime($item['date']);
                  echo $date_create->format('d/m/Y - H:i:s');
                  echo '<div class="pull-right">';
                    echo '<a href="'.BASEURL.'?page=all&action=del&id='.$item['id'].'" class="text-danger" title="Excluir Ticket"><i class="icon fa-trash"></i></a>';
                    echo '<a href="'.BASEURL.'?page=update&id='.$item['id'].'" class="text-info" title="Editar Ticket"><i class="icon fa-edit"></i></a>';
                    echo '<a href="'.BASEURL.'?page=all&action=open&id='.$item['id'].'" class="text-info" title="Visualizar Ticket"><i class="icon fa-eye"></i></a>';
                    echo '<a href="'.BASEURL.'?page=all&action=update&id='.$item['id'].'&flag='.$flag.'" class="text-success" title="Arquivar Ticket"><i class="icon fa-check"></i></a>';
                  echo '</div>';
                echo '</td>';
              echo '</tr>';
            }
          }
        ?>
      </tbody>
    </table>
  </div>
</div>
