<?php
  require_once('config.php');
  require_once(DBAPI);
  $db = new Database();

  include(HEADER_TEMPLATE);
  require_once(CONTROLLERS.'Tickets.php');
  $router = new TicketsController();

  if(!isset($_GET['page'])):
    $router->index();
  else:
    $page = $_GET['page'];
    switch($page){
      case 'home':
        $router->getTicket();
        break;

      case 'all':
        $router->getTicket();
        break;

      case 'new':
        $router->newTicket();
        break;

      case 'update':
        $router->updateTicket();
        break;

      default:
        $router->notFound();
        break;
    }
  endif;
  include(FOOTER_TEMPLATE);
?>
