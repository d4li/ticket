<?php
mysqli_report(MYSQLI_REPORT_STRICT);
class Database{
  private $host = DB_HOST;
  private $user = DB_USER;
  private $password = DB_PASSWORD;
  private $db_name = DB_NAME;
  private $conn;

  protected function opendb(){
    $this->conn = new mysqli($this->host, $this->user, $this->password, $this->db_name);
    return $this->conn;
  }//opendb

  protected function closedb(){
    mysqli_close($this->conn);
  }//closedb

  private function selectdb(){
    $db = mysqli_select_db($this->conn, $this->db_name);
    return $db;
  }//selectdb

  protected function statusdb(){
    if($this->selectdb()){
      echo '<span class="label label-success" role="info">Conectado ao banco: '.$this->db_name.'</span>';
    }else{
      echo '<span class="label label-danger" role="info">Erro ao conectar no banco: '.$this->db_name.'</span>';
    }
  }//statusdb

  protected function insertdb($table=null,$data=array()){
    $db = $this->opendb();
    if($this->selectdb()){
      $columns = '';
      $values = '';
      foreach ($data as $col => $val) {
        $columns .= ','.$col.' ';
        $values .= ' "'.$val.'",';
      }

      $columns = substr($columns, 1,-1);
      $values = substr($values, 1,-1);
      $sql = 'INSERT INTO '.$table.' ('.$columns.') VALUES('.$values.');';
      return $db->query($sql);
    }else{
      return null;
    }
  }//insertdb

  protected function getdb($table=null,$where=array()){
    $db = $this->opendb();
    if(!$where){
      $sql = "SELECT * FROM ".$table." WHERE flag=0";
    }else{
      $columns='';
      foreach($where as $col => $val){
        $columns .= $col.'='.$val.' AND ';
      }
      $columns = substr($columns, 0,-5);
      $sql = "SELECT * FROM ".$table." WHERE ".$columns;
    }

    $result = $db->query($sql);
    return $result;
  }//getdb

  protected function updatedb($table=null,$set=array(),$where=array()){
    $db = $this->opendb();

    $columns='';
    foreach($where as $col => $val){
      $columns .= $col.'='.$val.' AND ';
    }
    $columns = substr($columns, 0,-5);
    $data='';
    foreach ($set as $col => $val) {
      $data .= ' '.$col.'=\''.$val.'\',';
    }
    $data = substr($data, 1,-1);
    $sql = "UPDATE ".$table." SET ".$data." WHERE ".$columns;
    $result = $db->query($sql);
    return $result;
  }//updatedb

  protected function deletedb($table=null,$where=array()){
    $db = $this->opendb();

    $columns='';
    foreach($where as $col => $val){
      $columns .= $col.'='.$val.' AND ';
    }
    $columns = substr($columns, 0,-5);
    $sql = 'DELETE FROM '.$table.' WHERE '.$columns;

    $result = $db->query($sql);
    return $result;
  }//deletedb

  protected function setMessage($msg=null,$type=null,$clear=false){
    $_SESSION['getMessage'] = $msg;
    $_SESSION['typeMessage'] = $type;
  }

}
?>
