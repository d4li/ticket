<?php
class Ticket extends Database{
  private $table = 'tickets';

  protected function add($data=null) {
    if(!$data)
      return null;

    $this->insertdb($this->table, $data);
  }//add

  protected function get($where=array()) {
    if(!$where){
      $result = Database::getdb($this->table);
    }else{
      $result = Database::getdb($this->table,$where);
    }
    // if(!$result)
      // return $result->fetch_a();

    if($result->num_rows >= 1){
      return $result->fetch_all(MYSQLI_ASSOC);
    }
  }//get

  protected function update($set=array(),$where=array()) {
    if(!$set){
      return null;
    }else{
      $result = Database::updatedb($this->table,$set,$where);
    }

    if($result){
      // Database::setMessage('<strong>#'.$where['id'].'</strong> - Ticket atualizado com sucesso!','success',true);
      return $result;
    }
  }//update

  protected function delete($id=null) {
    if(!$id){
      return null;
    }else{
      $result = Database::deletedb($this->table,$id);
    }

    // Database::setMessage('<strong>#'.$id.'</strong> - Ticket deletado com sucesso!','success');
    return $result;
  }//delete
}
?>
