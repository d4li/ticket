'use strict';

(function(){
  // console.log('bla');
  // alert('loaded!');

  var forms = document.forms[0];
  if(forms !== undefined){
    for(var i = 0; i < forms.length; i++) {
      if(forms[i].value == "" || forms[i].value == " "){
        if(forms[i].type !== 'submit'){
          forms[i].addEventListener('blur',function(e){
            var el = e.target;
            if(el.value.length > 0){
              el.classList.remove('form_error');
              el.classList.add('form_ok');
              el.parentElement.children[1].classList.add('hide');
            }else{
              el.classList.remove('form_ok');
              el.classList.add('form_error');
              el.parentElement.children[1].classList.remove('hide');
            }

            var err = document.getElementsByClassName('form_error');
            var save = document.getElementsByClassName('save');
            if(save.length > 0){
              if(err.length >= 1){
                for(var i = 0; i < save.length; i++) {
                  save[i].classList.add('disabled');
                }
              }else{
                for(var i = 0; i < save.length; i++) {
                  save[i].classList.remove('disabled');
                }
              }
            }
          });
        }

        // }else{
        //   forms[i].classList.add('disabled');
        // }
      }else{
        console.log(forms[i].value);
      }
    }//for
  }
})(window);
