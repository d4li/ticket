<?php
  require_once(MODELS.'Ticket.php');
  class TicketsController extends Ticket{
    private $id;
    private $action;
    private $flag;

    public function index(){
      $this->getTicket();
    }

    public function getTicket(){
      global $tickets;

      if(isset($_GET['action']) && isset($_GET['id'])){
        $this->id = $_GET['id'];
        $this->action = $_GET['action'];
        switch ($this->action) {
          case 'del':
            Ticket::delete($this->id);
            break;

          case 'update':
            if(isset($_GET['flag'])){
              $this->flag = $_GET['flag'];
              $set = array('flag' => $this->flag);
              $where = array('id' => $this->id);
              Ticket::update($set,$where);
            }
            break;

          default:
            return false;
            break;
        }
      }

      if(isset($_GET['flag'])){
        $this->flag = $_GET['flag'];
        $where = array('flag' => $this->flag);
        $tickets = Ticket::get($where);
      }else{
        $tickets = Ticket::get();
      }
      include(VIEWS.'home.php');
    }//getTicket

    public function newTicket(){
      if(isset($_POST['save'])){
        $data = array(
          'firstname'=> ucfirst($_POST['firstname']),
          'lastname'=> ucfirst($_POST['lastname']),
          'description'=> $_POST['description'],
          'category'=> strtoupper($_POST['category']),
          'priority'=> strtoupper($_POST['priority']),
          'date' => date('Y-m-d H:i:s')
        );
        Ticket::add($data);
      }
      include(VIEWS.'new.php');
    }

    public function updateTicket(){
      global $tickets;
      if(isset($_GET['id']))
        $this->id = $_GET['id'];

      if(isset($_POST['save'])){
        $set = array(
          'firstname'=> ucfirst($_POST['firstname']),
          'lastname'=> ucfirst($_POST['lastname']),
          'description'=> $_POST['description'],
          'category'=> strtoupper($_POST['category']),
          'priority'=> strtoupper($_POST['priority']),
          'date' => date('Y-m-d H:i:s'),
          'flag' => $_POST['flag']
        );
        $where = array(
          'id' => $this->id
        );
        Ticket::update($set,$where);
      }

      $where = array(
        'id' => $this->id
      );
      $tickets = Ticket::get($where);
      include(VIEWS.'update.php');
    }//updateTicket

    public function notFound(){
      include(VIEWS.'notfound.php');
    }//notFound
  }
?>
