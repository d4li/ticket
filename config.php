<?php
	/** DATABASE */
	/** Host de conexão **/
	define('DB_HOST', 'localhost');

	/** Nome do banco de dados **/
	define('DB_NAME','dbTicket');

	/** Nome de usuário do banco de dados **/
	define('DB_USER','root');

	/** Senha do banco de dados **/
	define('DB_PASSWORD','');

	/** DIR **/
  /** Caminho absoluto do sistema **/
  if( !defined('ABSPATH') )
  	define('ABSPATH', dirname(__FILE__) . '/');

  /** Caminho absoluto do projeto **/
  if( !defined('BASEURL') )
    define('BASEURL', '/ticket/');

  /** DATABASE API **/
  if( !defined('DBAPI') )
    define('DBAPI', ABSPATH.'models/Database.php');

  /** TEMPLATES **/
  define('HEADER_TEMPLATE', ABSPATH . 'views/header.php');
  define('FOOTER_TEMPLATE', ABSPATH . 'views/footer.php');

  define('ASSETS', BASEURL . 'assets/');

  /** MVC **/
  define('MODELS', ABSPATH . 'models/');
  define('VIEWS', ABSPATH . 'views/');
  define('CONTROLLERS', ABSPATH . 'controllers/');

  /** TIMEZONE **/
  date_default_timezone_set('America/Sao_Paulo');
?>
